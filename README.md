### About
This is a modified version of [Talkit](https://github.com/rodobodolfo/Talkit)

It runs on [jointJS](http://www.jointjs.com/). It exports to JSON with game ready content.

### Node Types
* Text
* Choice
* Set Var
* Branch
* Topic
* Generic Node

### Export
It currently exports to json so you can import it into your game. Works with the Iris RPG Middleware

